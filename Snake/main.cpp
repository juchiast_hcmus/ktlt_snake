#include <iostream>
#include <thread>
#include <conio.h>
#include <string>
#include <assert.h>
#include <memory>
#include <ctime>

#include "console_utils.h"
#include "vector2.h"
#include "game_data.h"
#include "runtime_data.h"
#include "game_config.h"
#include "board.h"

using std::string;
const string MSSV = "1612847";

int getDelay(const GameData *game, const GameConfig *conf) {
	return conf->base_delay - game->speed * conf->delay_dec;
}
Vector2 moveForward(Vector2 &p, int d) {
	const static int dx[] = { 0, 1, 0, -1 };
	const static int dy[] = { -1, 0, 1, 0 };
	assert(0 <= d && d < 4);
	return { p.x + dx[d], p.y + dy[d] };
}

bool isValidDrection(int direction, Vector2 *snake, size_t snake_length) {
	return moveForward(snake[snake_length - 1], direction) != snake[snake_length - 2];
}

int getSnakeDirection(Vector2 *snake, size_t snake_length) {
	const static Vector2 p[] = { {0, -1}, {1, 0}, {0, 1}, {-1, 0} };
	Vector2 x = snake[snake_length - 1] - snake[snake_length - 2];
	for (int i = 0; i < 4; i++) {
		if (x == p[i]) return i;
	}
	assert(false);
	return -1;
}

bool checkBound(Vector2 a, Vector2 b) {
	return 0 <= a.x && a.x < b.x && 0 <= a.y && a.y < b.y;
}

void updateBoard(Board* board, const GameData *game, const RuntimeData *run) {
	std::memset(board->board, ' ', board->size);
	board->cursor = { 0, 0 };
	if (run->state == State::Playing || run->state == State::Dying || run->state == State::Gating) {
		for (int i = 0; i < board->w; i++) {
			at(board, i, 0) = '*';
			at(board, i, board->h - 1) = '*';
		}
		for (int i = 0; i < board->h; i++) {
			at(board, 0, i) = '*';
			at(board, board->w - 1, i) = '*';
		}
		if (game->door == Door::HasDoor) {
			at(board, game->door_pos.x + 1, game->door_pos.y + 1) = ' ';
		}
		if (game->door == Door::None) {
			at(board, game->food.x + 1, game->food.y + 1) = '+';
		}
		for (size_t i = 0; i < game->snake_length; i++) {
			at(board, game->snake[i].x + 1, game->snake[i].y + 1) = MSSV[i % MSSV.length()];
		}
		if (run->state == State::Dying) {
			for (size_t i = run->dying_count; i < game->snake_length; i++) {
				at(board, game->snake[i].x + 1, game->snake[i].y + 1) = '*';
			}
		}
	}
	else if (run->state == State::StartMenu) {
		string text[] = { "Nhan T de tai lai game", "Nhan phim bat ki de choi" };
		for (int y = 0; y < 2; y++) {
			for (int x = 0; x < (int)text[y].length(); x++) {
				at(board, x, y) = text[y][x];
			}
		}
	}
	else if (run->state == State::PlayAgainMenu) {
		string text[] = { "Nhan Y de choi lai", "Nhan phim bat ki de thoat" };
		for (int y = 0; y < 2; y++) {
			for (int x = 0; x < (int)text[y].length(); x++) {
				at(board, x, y) = text[y][x];
			}
		}
	}
	else if (run->state == State::LoadMenu) {
		string text[] = { "Nhap ten file de tai: " + run->text, "Co loi!" };
		for (int y = 0; y < 2; y++) {
			if (y == 1 && !run->io_fail) continue;
			for (int x = 0; x < (int)text[y].length(); x++) {
				at(board, x, y) = text[y][x];
			}
		}
		board->cursor = { (int)text[0].length(), 0 };
	}
	else if (run->state == State::SaveMenu) {
		string text[] = { "Nhap ten file de luu: " + run->text, "Co loi!" };
		for (int y = 0; y < 2; y++) {
			if (y == 1 && !run->io_fail) continue;
			for (int x = 0; x < (int)text[y].length(); x++) {
				at(board, x, y) = text[y][x];
			}
		}
		board->cursor = { (int)text[0].length(), 0 };
	}
}

void keyPressed(char key, GameData *game, RuntimeData *run, const GameConfig *conf) {
	key = tolower(key);
	if (run->state == State::Playing) {
		if (key == conf->key.up && !run->lock_direction) {
			run->direction = 0;
			run->lock_direction = true;
		}
		else if (key == conf->key.right && !run->lock_direction) {
			run->direction = 1;
			run->lock_direction = true;
		}
		else if (key == conf->key.down && !run->lock_direction) {
			run->direction = 2;
			run->lock_direction = true;
		}
		else if (key == conf->key.left && !run->lock_direction) {
			run->direction = 3;
			run->lock_direction = true;
		}
		else if (key == conf->key.load) {
			run->state = State::LoadMenu;
		}
		else if (key == conf->key.pause) {
			run->state = State::SaveMenu;
		}
	}
	else if (run->state == State::StartMenu) {
		if (key == conf->key.load) {
			run->state = State::LoadMenu;
		}
		else {
			run->state = State::Playing;
		}
	}
	else if (run->state == State::PlayAgainMenu) {
		if (key == conf->key.yes) {
			{
				auto tmp = newGameData();
				*game = *tmp;
				delete tmp;
			}
			run->direction = getSnakeDirection(game->snake, game->snake_length);
			run->state = State::Playing;
		}
		else {
			run->state = State::Ended;
		}
	}
	else if (run->state == State::LoadMenu) {
		if (key == conf->key.enter) {
			run->io_fail = readGameData(run->text, game);
			run->direction = getSnakeDirection(game->snake, game->snake_length);
			if (!run->io_fail) {
				run->state = State::Playing;
			}
		}
		else if (key == conf->key.del) {
			if (!run->text.empty()) {
				run->text.pop_back();
			}
		}
		else if (isascii(key)) {
			run->text += key;
		}
	}
	else if (run->state == State::SaveMenu) {
		if (key == conf->key.enter) {
			run->io_fail = writeGameData(run->text, game);
			if (!run->io_fail) {
				run->state = State::Playing;
			}
		}
		else if (key == conf->key.del) {
			if (!run->text.empty()) {
				run->text.pop_back();
			}
		}
		else if (isascii(key)) {
			run->text += key;
		}
	}
}
void threadFunc(GameData *game, RuntimeData *run, const GameConfig *conf) {
	while (run->state != State::Ended) {
		char k = _getch();
		keyPressed(k, game, run, conf);
	}
}

void Repaint(Board* board) {
	for (int i = 0; i < board->w; i++) {
		for (int j = 0; j < board->h; j++) {
			if (at(board, i, j) != cache_at(board, i, j)) {
				move_cursor(i, j);
				std::putchar(at(board, i, j));
			}
		}
	}
	move_cursor(board->cursor.x, board->cursor.y);
	std::memcpy(board->cache, board->board, board->size);
}

void updateGame(GameData *game, RuntimeData *run, const GameConfig *conf) {
	if (run->state == State::Playing) {
		if (!isValidDrection(run->direction, game->snake, game->snake_length)) {
			run->direction = getSnakeDirection(game->snake, game->snake_length);
		}
		auto tail = game->snake[0];
		for (size_t i = 0; i < game->snake_length - 1; i++) {
			game->snake[i] = game->snake[i + 1];
		}
		game->snake[game->snake_length - 1] = moveForward(game->snake[game->snake_length - 1], run->direction);
		run->lock_direction = false;

		bool eat_self = false;
		for (size_t i = 0; i < game->snake_length - 1; i++) {
			if (game->snake[i] == game->snake[game->snake_length - 1]) {
				eat_self = true;
				break;
			}
		}
		if (game->snake[game->snake_length - 1] == game->door_pos) {
			run->bk_length = game->snake_length;
			run->state = State::Gating;
		} else if (eat_self || !checkBound(game->snake[game->snake_length - 1], game->size)) {
			run->state = State::Dying;
			run->dying_count = game->snake_length - 1;
		}

		if (game->snake[game->snake_length - 1] == game->food) {
			game->snake_length++;
			game->eaten_foods_count++;
			for (size_t i = game->snake_length - 1; i > 0; i--) {
				game->snake[i] = game->snake[i - 1];
			}
			game->snake[0] = tail;

			if (game->eaten_foods_count >= conf->food_limit) {
				game->speed++;
				game->eaten_foods_count = 0;
				game->door = Door::HasDoor;
				game->door_pos.x = std::rand() % (game->size.x - 10) + 5;
				game->food = { -1, -1 };
			}
			else {
				game->food = newRandomFood(game->snake, game->snake_length, game->size);
			}
		}
	}
	else if (run->state == State::Dying) {
		if (run->dying_count >= 0) {
			run->dying_count--;
		}
		else {
			run->state = State::PlayAgainMenu;
		}
	}
	else if (run->state == State::Gating) {
		run->direction = getSnakeDirection(game->snake, game->snake_length);
		for (size_t i = 0; i < game->snake_length - 1; i++) {
			game->snake[i] = game->snake[i + 1];
		}
		game->snake_length--;

		if (game->snake_length <= 1) {
			game->snake_length = run->bk_length;
			game->door = Door::None;
			for (size_t i = 0; i < game->snake_length; i++) {
				game->snake[i] = { (int)i, 0 };
			}
			run->state = State::Playing;
			run->direction = getSnakeDirection(game->snake, game->snake_length);
			game->food = newRandomFood(game->snake, game->snake_length, game->size);
		}
	}
}

int main() {
	std::srand(std::time(0));
	fix_console_window();

	auto conf = newGameConfig();
	auto game = newGameData();
	auto run = newRuntimeData();
	auto board = newBoard(game->size.x, game->size.y);

	std::thread t1(threadFunc, game, run, conf);

	while (run->state != State::Ended) {
		updateBoard(board, game, run);
		Repaint(board);
		if (run->io_fail) {
			std::this_thread::sleep_for(std::chrono::milliseconds(1000));
			run->io_fail = false;
			run->state = State::Playing;
		}
		updateGame(game, run, conf);
		std::this_thread::sleep_for(std::chrono::milliseconds(getDelay(game, conf)));
	}

	t1.join();
	delete game;
	delete run;
	deleteBoard(board);
	delete conf;
	return 0;
}