#pragma once
#include <cstdlib>
#include "vector2.h"
#include <iostream>
#include <fstream>
#include <string>
using namespace std;

Vector2 newRandomFood(const Vector2 *snake, size_t len, const Vector2& size) {
	Vector2 food;
	bool b;
	do {
		food.x = std::rand() % size.x;
		food.y = std::rand() % size.y;
		b = false;
		for (size_t i = 0; i < len; i++) {
			if (snake[i] == food) {
				b = true;
				break;
			}
		}
	} while (b);
	return food;
}
enum class Door {
	None,
	HasDoor,
};
struct GameData {
	Vector2 size;
	Vector2 snake[100];
	size_t snake_length;
	int eaten_foods_count;
	Vector2 food;
	Door door;
	Vector2 door_pos;
	int speed;
};
GameData* newGameData() {
	auto p = new GameData;
	p->eaten_foods_count = 0;
	p->snake_length = 6;
	for (size_t i = 0; i < p->snake_length; i++) {
		p->snake[i] = {(int)i, 0};
	}
	p->size = {110, 28};
	p->food = newRandomFood(p->snake, p->snake_length, p->size);
	p->door = Door::None;
	p->speed = 0;
	p->door_pos = {0, p->size.y};
	return p;
}
bool readGameData(const std::string& path, GameData *game) {
	ifstream f(path, ios::binary);
	if (!f) return true;
	f.read((char*)game, sizeof(GameData));
	return f.fail();
}
bool writeGameData(const std::string& path, const GameData* game) {
	ofstream f(path, ios::binary);
	if (!f) return true;
	f.write((char*)game, sizeof(GameData));
	return f.fail();
}