#pragma once

struct Vector2 { int x, y; };
bool operator==(Vector2 a, Vector2 b) {
	return a.x == b.x && a.y == b.y;
}
bool operator!=(Vector2 a, Vector2 b) {
	return a.x != b.x || a.y != b.y;
}
Vector2 operator-(Vector2 a, Vector2 b) {
	return {a.x - b.x, a.y - b.y};
}