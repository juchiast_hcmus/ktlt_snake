#pragma once
#include <string>
using std::string;

enum class State {
	Dying,
	Gating,
	Playing,
	Ended,
	StartMenu,
	PlayAgainMenu,
	SaveMenu,
	LoadMenu,
};
struct RuntimeData {
	State state;
	string text;
	int direction;
	bool lock_direction;
	bool io_fail;
	int dying_count;
	int bk_length;
	Vector2 cursor;
};

RuntimeData* newRuntimeData() {
	auto p = new RuntimeData;
	p->state = State::StartMenu;
	p->text = "";
	p->direction = 1;
	p->lock_direction = false;
	p->io_fail = false;
	p->bk_length = 0;
	p->cursor = { 1, 1 };
	return p;
}